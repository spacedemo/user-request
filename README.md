# Composant "User Request"
Le rôle du composant "User Request" est d'assurer la gestion des demandes de
prise de vue formulées par les utilisateurs finaux du système. Ce composant
propose une interface Web permettant d'adresser et suivre les demandes et d'un
serveur permettant de collecter l'ensemble des demandes des utilisateurs et de
communiquer avec les composants de programmation de la mission.

## Interface Web

### Périmètre fonctionnel
L'interface Web propose plusieurs fonctionnalités:
- Dépot de demande: un utilisateur peut adresser une demande de programmation
au système en ciblant le centre de la zone à acquérir et en communiquant
l'adresse électronique avec laquelle il souhaite recevoir une notification
lorsque l'acquisition a été réalisée.
- A compléter...

### Conception
L'interface Web du service de gestion des demandes utilisateurs est réalisée
avec la technologie React.

A compléter...

## Serveur de demandes
A compléter...
