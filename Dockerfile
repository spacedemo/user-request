FROM openjdk:8-jre-slim

COPY usr-backend/target/usr-backend.jar .

CMD ["java", "-jar", "usr-backend.jar"]
