import React from 'react'
import { Map, TileLayer } from 'react-leaflet'

/**
 * Render the deposit view.
 */
class DepositView extends React.Component {
  /**
   * Constructor of the view
   * @param {Object[]} props Properties of the view
   */
  constructor (props) {
    super(props)

    // Create the client proxy
    //this.userRequestService = new UserRequestService()

    // Bind methods of the view
    this.changeEmail = this.changeEmail.bind(this)
    this.changeTarget = this.changeTarget.bind(this)
    this.isSubmitDisabled = this.isSubmitDisabled.bind(this)

    // Initialize the state on Toulouse
    this.state = {
      lat: 43.6,
      lng: 1.45,
      zoom: 9,
      target: '',
      email: ''
    }
  }

  /**
   * Change the value of the email.
   * @param {Event} evt Email change event
   * @return {void}
   */
  changeEmail (evt) {
    this.setState({
      email: evt.target.value
    })
  }

  /**
   * Handle a cick on the map to define a new target.
   * @return {void}
   */
  changeTarget() {
    this.setState({
      target: 'my target is set'
    })
  }

  /**
   * Check if the submit of the request should be disabled.
   * @return {boolean} Is the submit button disabled?
   */
  isSubmitDisabled() {
    const target = this.state.target
    const email = this.state.email
    return target === '' || email === ''
  }

  /**
   * Render the view.
   */
  render () {
    const position = [this.state.lat, this.state.lng]
    const email = this.state.email
    const submitDisabled = this.isSubmitDisabled()
    console.log(email)
    return (
      <div>
        <input
          type="text"
          size="40"
          name="email"
          placeholder="Your email address"
          value={email}
          onChange={this.changeEmail}/>
        <input
          type="text"
          size="20"
          name="position"
          placeholder="No position defined"
          value={this.state.target}
          disabled/>
        <button disabled={submitDisabled}>Submit</button>
        <Map
          center={position}
          zoom={this.state.zoom}
          onClick={this.changeTarget}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
        </Map>
      </div>
    )
  }
}

export default DepositView
