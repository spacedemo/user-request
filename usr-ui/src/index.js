import React from 'react' // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom'
import DepositView from './view/DepositView' // eslint-disable-line no-unused-vars

ReactDOM.render(<DepositView />, document.getElementById('root'))
