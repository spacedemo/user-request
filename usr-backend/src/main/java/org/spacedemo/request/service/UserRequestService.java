package org.spacedemo.request.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.spacedemo.request.dto.Position;
import org.spacedemo.request.dto.PositionDate;
import org.spacedemo.request.dto.UserRequest;
import org.spacedemo.request.entity.UserRequestEntity;
import org.spacedemo.request.repository.UserRequestRepository;
import org.spacedemo.request.utils.UserRequestConverter;
import org.springframework.stereotype.Service;

/**
 * 
 * @author MROUANET
 *
 */
@Service
public class UserRequestService {

	private UserRequestRepository repositoryUserRequest;

	/**
	 * 
	 * @param repositoryUserRequest
	 */
	public UserRequestService(UserRequestRepository repositoryUserRequest) {
		this.repositoryUserRequest = repositoryUserRequest;
	}

	/**
	 * 
	 * @param userRequestId
	 */
	public void deleteUserRequestById(@NotNull String userRequestId) {
		repositoryUserRequest.deleteById(UserRequestConverter.toEntityId(userRequestId));
	}

	/**
	 * 
	 * @param emailUser
	 * @param positionDatee
	 * @return
	 */
	public UserRequest createUserRequest(String emailUser, PositionDate positionDatee) {
		// Création de l'entité à sauver
		UserRequestEntity entite = new UserRequestEntity();
		entite.setCreationDate(positionDatee.getDate().toString());
		Position position = new Position();
		position.setLatitude(positionDatee.getPosition().getLatitude());
		position.setLongitude(positionDatee.getPosition().getLongitude());
		entite.setPosition(position);
		entite.setEmailUser(emailUser);
		// Sauvegarde de l'entité
		UserRequestEntity entiteSauvee = repositoryUserRequest.save(entite);
		// Renvoi du DTO correspondant à l'entité sauvée
		return UserRequestConverter.toUserRequest(entiteSauvee);
	}

	/**
	 * 
	 * @param userRequestId
	 * @return
	 */
	public UserRequest getUserRequestById(@NotNull String userRequestId) {
		return repositoryUserRequest.findById(UserRequestConverter.toEntityId(userRequestId))
				.map(entity -> UserRequestConverter.toUserRequest(entity)).orElseGet(() -> null);
	}

	/**
	 * 
	 * @return
	 */
	public List<UserRequest> findAllRequests() {
		return repositoryUserRequest.findAll().stream().map(entity -> UserRequestConverter.toUserRequest(entity))
				.collect(Collectors.toList());
	}
}
