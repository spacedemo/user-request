package org.spacedemo.request.service;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.spacedemo.request.config.MailConfig;
import org.spacedemo.request.dto.UserRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mailjet.client.Base64;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Emailv31;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MailService {

	private MailConfig configMail;

	public MailService(MailConfig configMail) {
		this.configMail = configMail;
	}

	/**
	 * 
	 * @param userRequestId
	 * @param image
	 * @return
	 */
	public HttpStatus sendMail(UserRequest usrRequest, MultipartFile image) {
		try {
			MailjetClient clientMail = new MailjetClient(configMail.getMailClientApiKeyPublic(),
					configMail.getMailClientApiKeyPrivate());
			MailjetRequest request = new MailjetRequest(
					Emailv31.resource).property(Emailv31.MESSAGES,
							new JSONArray().put(new JSONObject()
									// From
									.put(Emailv31.Message.FROM,
											new JSONObject().put(Emailv31.Message.EMAIL, "spacedemo@gmail.com")
													.put(Emailv31.Message.NAME, "Spacedemo"))
									// To
									.put(Emailv31.Message.TO,
											new JSONArray().put(new JSONObject()
													.put(Emailv31.Message.EMAIL, usrRequest.getEmailUser())
													.put(Emailv31.Message.NAME, "")))
									// Sujet du mail
									.put(Emailv31.Message.SUBJECT, "").put(Emailv31.Message.TEXTPART, "")
									// Pièce jointe. Le contenu doit être encodé en base 64
									.put(Emailv31.Message.ATTACHMENTS,
											new JSONArray().put(new JSONObject().put("Filename", image.getName())
													.put("Content-type", image.getContentType())
													.put("Content", Base64.encode(image.getBytes()))))));
			log.debug("Requête d'envoi de mail \n" + request.toString());
			MailjetResponse response = clientMail.post(request);
			return HttpStatus.valueOf(response.getStatus());
		} catch (MailjetException | MailjetSocketTimeoutException | JSONException | IOException e) {
			log.error("Erreur lors de l'envoi du mail : ", e);
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}
}
