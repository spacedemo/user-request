package org.spacedemo.request.utils;

import org.bson.types.ObjectId;
import org.spacedemo.request.dto.Position;
import org.spacedemo.request.dto.UserRequest;
import org.spacedemo.request.entity.UserRequestEntity;

/**
 * 
 * @author MROUANET
 *
 */
public class UserRequestConverter {

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static UserRequest toUserRequest(UserRequestEntity source) {
		UserRequest usr = new UserRequest();
		usr.setId(source.getId().toHexString());
		usr.setCreationDate(source.getCreationDate());
		usr.setEmailUser(source.getEmailUser());
		Position pos = new Position();
		pos.setLatitude(source.getPosition().getLatitude());
		pos.setLongitude(source.getPosition().getLongitude());
		usr.setPosition(pos);
		return usr;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public static ObjectId toEntityId(String id) {
		return new ObjectId(id);
	}
}
