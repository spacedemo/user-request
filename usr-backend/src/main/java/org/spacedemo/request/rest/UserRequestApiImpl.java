package org.spacedemo.request.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.spacedemo.request.dto.UserRequest;
import org.spacedemo.request.dto.UserRequestCreation;
import org.spacedemo.request.service.MailService;
import org.spacedemo.request.service.UserRequestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UserRequestApiImpl implements UserRequestApi {

	private MailService serviceMail;

	private UserRequestService serviceUserRequest;

	/**
	 * Constructeur d'injection
	 * 
	 * @param serviceMail        injection du service de mail
	 * @param serviceUserRequest injection du service User Request
	 */
	public UserRequestApiImpl(MailService serviceMail, UserRequestService serviceUserRequest) {
		this.serviceMail = serviceMail;
		this.serviceUserRequest = serviceUserRequest;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseEntity<List<UserRequest>> userRequestGet() {
		return ResponseEntity.ok(this.serviceUserRequest.findAllRequests());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseEntity<Void> userRequestUserRequestIdDelete(String userRequestId) {
		// Suppression de la requete associée
		serviceUserRequest.deleteUserRequestById(userRequestId);
		return ResponseEntity.ok().build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseEntity<UserRequest> userRequestUserRequestIdGet(String userRequestId) {
		// Récupération de la User Request correspondante
		UserRequest request = serviceUserRequest.getUserRequestById(userRequestId);
		// Gestion du code retour en fonction de la présence de la User Request en base
		return Optional.ofNullable(request).map(usr -> ResponseEntity.ok(usr))
				.orElseGet(() -> ResponseEntity.notFound().build());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseEntity<UserRequest> userRequestSendImagePost(@RequestParam("userRequestId") String userRequestId,
			@RequestBody @Valid MultipartFile image) {
		// Récupération de la user request concernée
		UserRequest usrRequest = serviceUserRequest.getUserRequestById(userRequestId);
		// Envoi du mail.
		HttpStatus statut = serviceMail.sendMail(usrRequest, image);
		return ResponseEntity.status(statut).body(usrRequest);
	}

	@Override
	public ResponseEntity<UserRequest> userRequestPost(UserRequestCreation creation) {
		UserRequest usr = serviceUserRequest.createUserRequest(creation.getEmailUser(), creation.getPositionDatee());
		return Optional.ofNullable(usr).map(request -> ResponseEntity.ok(usr))
				.orElseGet(() -> ResponseEntity.notFound().build());
	}

}
