package org.spacedemo.request.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.spacedemo.request.dto.PositionDate;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserRequestCreation
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-05-25T13:54:23.865808300+02:00[Europe/Paris]")

public class UserRequestCreation  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("email_user")
  private String emailUser;

  @JsonProperty("positionDatee")
  private PositionDate positionDatee;

  public UserRequestCreation emailUser(String emailUser) {
    this.emailUser = emailUser;
    return this;
  }

  /**
   * Get emailUser
   * @return emailUser
  */
  @ApiModelProperty(value = "")


  public String getEmailUser() {
    return emailUser;
  }

  public void setEmailUser(String emailUser) {
    this.emailUser = emailUser;
  }

  public UserRequestCreation positionDatee(PositionDate positionDatee) {
    this.positionDatee = positionDatee;
    return this;
  }

  /**
   * Get positionDatee
   * @return positionDatee
  */
  @ApiModelProperty(value = "")

  @Valid

  public PositionDate getPositionDatee() {
    return positionDatee;
  }

  public void setPositionDatee(PositionDate positionDatee) {
    this.positionDatee = positionDatee;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserRequestCreation userRequestCreation = (UserRequestCreation) o;
    return Objects.equals(this.emailUser, userRequestCreation.emailUser) &&
        Objects.equals(this.positionDatee, userRequestCreation.positionDatee);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emailUser, positionDatee);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserRequestCreation {\n");
    
    sb.append("    emailUser: ").append(toIndentedString(emailUser)).append("\n");
    sb.append("    positionDatee: ").append(toIndentedString(positionDatee)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

