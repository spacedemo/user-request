package org.spacedemo.request.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Intervalle de temps
 */
@ApiModel(description = "Intervalle de temps")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-05-25T13:54:23.865808300+02:00[Europe/Paris]")

public class Interval  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("begin")
  private LocalDateTime begin;

  @JsonProperty("end")
  private LocalDateTime end;

  public Interval begin(LocalDateTime begin) {
    this.begin = begin;
    return this;
  }

  /**
   * Date de début de l'intervalle
   * @return begin
  */
  @ApiModelProperty(value = "Date de début de l'intervalle")

  @Valid

  public LocalDateTime getBegin() {
    return begin;
  }

  public void setBegin(LocalDateTime begin) {
    this.begin = begin;
  }

  public Interval end(LocalDateTime end) {
    this.end = end;
    return this;
  }

  /**
   * Date de fin de l'intervalle
   * @return end
  */
  @ApiModelProperty(value = "Date de fin de l'intervalle")

  @Valid

  public LocalDateTime getEnd() {
    return end;
  }

  public void setEnd(LocalDateTime end) {
    this.end = end;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Interval interval = (Interval) o;
    return Objects.equals(this.begin, interval.begin) &&
        Objects.equals(this.end, interval.end);
  }

  @Override
  public int hashCode() {
    return Objects.hash(begin, end);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Interval {\n");
    
    sb.append("    begin: ").append(toIndentedString(begin)).append("\n");
    sb.append("    end: ").append(toIndentedString(end)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

