package org.spacedemo.request.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.spacedemo.request.dto.Position;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Demande d&#39;image provenant d&#39;un utilisateur
 */
@ApiModel(description = "Demande d'image provenant d'un utilisateur")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-05-25T13:54:23.865808300+02:00[Europe/Paris]")

public class UserRequest  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("id")
  private String id;

  @JsonProperty("position")
  private Position position;

  @JsonProperty("creation_date")
  private String creationDate;

  @JsonProperty("email_user")
  private String emailUser;

  public UserRequest id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifiant unique de la User Request
   * @return id
  */
  @ApiModelProperty(value = "Identifiant unique de la User Request")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public UserRequest position(Position position) {
    this.position = position;
    return this;
  }

  /**
   * Get position
   * @return position
  */
  @ApiModelProperty(value = "")

  @Valid

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  public UserRequest creationDate(String creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  /**
   * Date de création de la User Request
   * @return creationDate
  */
  @ApiModelProperty(value = "Date de création de la User Request")


  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public UserRequest emailUser(String emailUser) {
    this.emailUser = emailUser;
    return this;
  }

  /**
   * Email de l'utilisateur à l'origine de la User Request
   * @return emailUser
  */
  @ApiModelProperty(value = "Email de l'utilisateur à l'origine de la User Request")


  public String getEmailUser() {
    return emailUser;
  }

  public void setEmailUser(String emailUser) {
    this.emailUser = emailUser;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserRequest userRequest = (UserRequest) o;
    return Objects.equals(this.id, userRequest.id) &&
        Objects.equals(this.position, userRequest.position) &&
        Objects.equals(this.creationDate, userRequest.creationDate) &&
        Objects.equals(this.emailUser, userRequest.emailUser);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, position, creationDate, emailUser);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserRequest {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    position: ").append(toIndentedString(position)).append("\n");
    sb.append("    creationDate: ").append(toIndentedString(creationDate)).append("\n");
    sb.append("    emailUser: ").append(toIndentedString(emailUser)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

