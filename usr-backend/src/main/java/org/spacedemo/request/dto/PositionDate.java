package org.spacedemo.request.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import org.spacedemo.request.dto.Position;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Position datée
 */
@ApiModel(description = "Position datée")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-05-25T13:54:23.865808300+02:00[Europe/Paris]")

public class PositionDate  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("position")
  private Position position;

  @JsonProperty("date")
  private LocalDateTime date;

  public PositionDate position(Position position) {
    this.position = position;
    return this;
  }

  /**
   * Get position
   * @return position
  */
  @ApiModelProperty(value = "")

  @Valid

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  public PositionDate date(LocalDateTime date) {
    this.date = date;
    return this;
  }

  /**
   * Date de la position
   * @return date
  */
  @ApiModelProperty(value = "Date de la position")

  @Valid

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PositionDate positionDate = (PositionDate) o;
    return Objects.equals(this.position, positionDate.position) &&
        Objects.equals(this.date, positionDate.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(position, date);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PositionDate {\n");
    
    sb.append("    position: ").append(toIndentedString(position)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

