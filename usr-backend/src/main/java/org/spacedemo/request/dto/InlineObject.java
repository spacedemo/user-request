package org.spacedemo.request.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.core.io.Resource;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * InlineObject
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-05-25T13:54:23.865808300+02:00[Europe/Paris]")

public class InlineObject  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("userRequestId")
  private String userRequestId;

  @JsonProperty("image")
  private Resource image;

  public InlineObject userRequestId(String userRequestId) {
    this.userRequestId = userRequestId;
    return this;
  }

  /**
   * Get userRequestId
   * @return userRequestId
  */
  @ApiModelProperty(value = "")


  public String getUserRequestId() {
    return userRequestId;
  }

  public void setUserRequestId(String userRequestId) {
    this.userRequestId = userRequestId;
  }

  public InlineObject image(Resource image) {
    this.image = image;
    return this;
  }

  /**
   * Get image
   * @return image
  */
  @ApiModelProperty(value = "")

  @Valid

  public Resource getImage() {
    return image;
  }

  public void setImage(Resource image) {
    this.image = image;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineObject inlineObject = (InlineObject) o;
    return Objects.equals(this.userRequestId, inlineObject.userRequestId) &&
        Objects.equals(this.image, inlineObject.image);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userRequestId, image);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineObject {\n");
    
    sb.append("    userRequestId: ").append(toIndentedString(userRequestId)).append("\n");
    sb.append("    image: ").append(toIndentedString(image)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

