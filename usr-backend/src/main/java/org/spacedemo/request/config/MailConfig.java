package org.spacedemo.request.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class MailConfig {

    @Value("${org.spacedemo.request.mail.client.api.key.public}")
    private String mailClientApiKeyPublic;

    @Value("${org.spacedemo.request.mail.client.api.key.private}")
    private String mailClientApiKeyPrivate;

}
