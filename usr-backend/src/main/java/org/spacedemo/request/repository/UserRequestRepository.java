package org.spacedemo.request.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.bson.types.ObjectId;
import org.spacedemo.request.dto.UserRequest;
import org.spacedemo.request.entity.UserRequestEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author MROUANET
 *
 */
@Repository
public interface UserRequestRepository extends MongoRepository<UserRequestEntity, ObjectId> {

	/**
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	ResponseEntity<List<UserRequest>> findByCreationDateGreaterThanEqualAndCreationDateLessThanEqual(
			LocalDateTime begin, LocalDateTime end);

}
