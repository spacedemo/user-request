package org.spacedemo.request;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserRequestBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserRequestBackendApplication.class, args);
    }

}
