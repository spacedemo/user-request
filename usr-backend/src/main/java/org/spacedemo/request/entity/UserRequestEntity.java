package org.spacedemo.request.entity;

import org.bson.types.ObjectId;
import org.spacedemo.request.dto.Position;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
@Getter
@Setter
public class UserRequestEntity {

	@Id
	private ObjectId id;

	private Position position;

	private String creationDate;

	private String emailUser;
}
